<?php

// Константа указывает на корень проекта
define('ROOT', __DIR__ . '/..');
// Константа указывает на корень приложения
define('APP', '/home/c/ck92417/app');
// Константа указывает на отображения приложения
define('VIEWS', '/home/c/ck92417/resources/views');
// Константа для формирования ссылок на сайт
define('URL', 'http://' . $_SERVER['HTTP_HOST'] . '/');

// Выводим все ошибки
error_reporting(E_ALL);
ini_set("display_errors", 1);

require '/home/c/ck92417/vendor/autoload.php';

// Запускаем приложение
$app = new \App\Core\Application();
$app->handle();
