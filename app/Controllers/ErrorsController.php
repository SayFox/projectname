<?php

namespace App\Controllers;


/**
 * Контроллер для страниц с ошибками.
 */
class ErrorsController 
{
    /**
     * Страница с ошибкой 404 – страница не найдена.
     */
    public function indexAction($message = null)
    {
        require VIEWS . '/errors/index.php';
    }
}
