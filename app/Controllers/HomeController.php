<?php

namespace App\Controllers;
require '/home/c/ck92417/vendor/autoload.php';

/**
 * Класс для статических страниц
 */
class HomeController
{
    /**
     * Страница "Главная".
     *
     * @link /home/index
     */
    public function indexAction()
    {
      require VIEWS . '/home/index.php';
    }

    /**
     * Возвращает JSON-документ.
     *
     * @link /home/add
     */
    public function addAction()
    {
			$mysqli = new \mysqli(
            'localhost',  /* Хост, к которому мы подключаемся */
            'ck92417_sayfox',       /* Имя пользователя */
            '123456789',   /* Используемый пароль */
            'ck92417_sayfox');     /* База данных для запросов по умолчанию */
            if ($mysqli->connect_errno) {
			    printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
			    exit();
			}
			
    	
      if ($_POST) 
      {
	        if (filter_var($_POST['longurl'], FILTER_VALIDATE_URL))
	        {
	        	
	         $link = $mysqli->query("SELECT * FROM `url` WHERE `longurl` = '$_POST[longurl]' ");
	          if ($link->num_rows == 0) {
	            // Формируем хэш для URL.
	            $hash = hash('crc32', $_POST['longurl']);
	            $mysqli->query("INSERT INTO `url` (`hash`, `longurl`) VALUES ( '$hash' , '$_POST[longurl]' )");
	        }
	        $link = $mysqli->query("SELECT * FROM `url` WHERE `longurl` = '$_POST[longurl]' ");
	        echo  json_encode(array('url' =>'http://ck92417.tmweb.ru/home/red/' . mysqli_fetch_array($link, MYSQLI_ASSOC)['hash']));
	        exit;
	          } else {
	            echo json_encode(array('error' => 'No valid link'));
	            exit;
	          }
        }

    }

    /**
     *
     *
     * @link /home/red/{slug}
     */
    public function redAction($hash)
    {
      $mysqli = new \mysqli(
            'localhost',  /* Хост, к которому мы подключаемся */
            'ck92417_sayfox',       /* Имя пользователя */
            '123456789',   /* Используемый пароль */
            'ck92417_sayfox');     /* База данных для запросов по умолчанию */
            if ($mysqli->connect_errno) {
			    printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
			    exit();
			}
        if ($mysqli->query("SELECT * FROM `url` WHERE `hash` = '$hash' ")->num_rows != 0){
        	$link = $mysqli->query("SELECT * FROM `url` WHERE `hash` = '$hash' ");
          header('Location:' . mysqli_fetch_array($link, MYSQLI_ASSOC)['longurl']);
      } else header('Location:/errors/index.php');

    }
}
